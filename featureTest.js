let Twit = require('twit')
let config = require('./config')
let fs = require('fs')
let path = require('path')

let T = new Twit({
  consumer_key: config.consumer_key,
  consumer_secret: config.consumer_secret,
  access_token: config.access_token_key,
  access_token_secret: config.access_token_secret,
})

const artistHandleMapping = {
  room208: '@room__208',
  dystopiakid: '@dystopia_kid',
  weltschmerz: '@inweltschmerz',
  quintin: '@quintin_q_q',
  ghostnote: '@ghst_note',
  mingkurray: '@mingkurray',
}

module.exports.postToTwitter = function (options) {
  const defaultOptions = { isDryRun: false }
  options = {
    ...defaultOptions,
    ...options,
  }
  const directoryPath = path.join(__dirname, 'Snapshots')
  //passing directoryPath and callback function
  fs.readdir(directoryPath, function (err, screenshots) {
    //handling error
    if (err) {
      return console.log('Unable to scan directory: ' + err)
    }
    if (screenshots.length === 0) {
      console.log('there are no more images in the folder.')
      return
    }

    let randomFile = Math.floor(Math.random() * screenshots.length)
    let imageToPost = screenshots[randomFile]
    let image_path = path.join(__dirname, '/Snapshots/' + imageToPost)
    let b64content = fs.readFileSync(image_path, { encoding: 'base64' })

    // Expects file to be titled like: room208_img545645.png
    let artistNameFromFile = imageToPost.split('_')[0] || ''
    const artistName = artistHandleMapping[artistNameFromFile] || ''

    let creditString = artistName !== '' ? `Image by ${artistName}` : ''
    // Keep this console.log enabled, it returns the name of the file needed for delete.
    console.log(imageToPost)
    console.log(`artist name is:${artistName}`)

    if (options.isDryRun) {
      console.log('This is a dry run, no image was posted')
      return
    } else {
      //first we must post the media to Twitter
      // T.post("media/upload", { media_data: b64content }, function(
      //   err,
      //   data,
      //   response
      // ) {
      //   // now we can assign alt text to the media, for use by screen readers and
      //   // other text-based presentations and interpreters
      //   var mediaIdStr = data.media_id_string;
      //   var altText = "Glitch artwork bot, posting images and artwork <3";
      //   var meta_params = { media_id: mediaIdStr, alt_text: { text: altText } };
      //   T.post("media/metadata/create", meta_params, function(
      //     err,
      //     data,
      //     response
      //   ) {
      //     if (!err) {
      //       // now we can reference the media and post a tweet (media will attach to the tweet)
      //       var params = {
      //         status: `${creditString}
      //         #glitch #glitchartist #glitchart #digitalart`,
      //         media_ids: [mediaIdStr]
      //       };
      //       T.post("statuses/update", params, function(err, data, response) {
      //         console.log(`Successfully posted the ${creditString}`);
      //       });
      //     }
      //   });
      // });
      // // this is for removing the file once it's been posted.
      // try {
      //   fs.unlinkSync(`${directoryPath}/${imageToPost}`);
      //   console.log("removed");
      // } catch (err) {
      //   console.log(`That didn't work, here's the error: ${err}`);
      // }
      // return imageToPost;
    }
  })
}
