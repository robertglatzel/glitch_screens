# Glitch Screens

A twitter bot that posts glitch artwork and screen-captures from artists in the community.
A new image is posted every 12 hours.

Follow Glitch Screens: 
https://twitter.com/glitch_screens

## How it works

The setup is relatively straight forward.
I have the most basic digital ocean droplet hosting the repository and applications to run the bot.
The two main applications post images to twitter and syncs contents from a google drive folder to a folder in the droplet. 
Using tmux, two instances are running on an infinite loop, one syncing new images between folders every 2 days, and another that posts to twitter every 12 hours.

After each successful post to twitter, the image is deleted from the google drive account. (The droplet can only store 25gb at a time. This will ensure that I am clearing up space once a post is successful.)

I have now incorporated imagemagick to help condense the file sizes. Since Twitter only allows images to be less then 5mb, I have to resize the images in the Snapshots folder upon sync. The mogrify command allows me to resize images in place. 


Submission form has been created. Artists can now submit their artwork there, along with their twitter handle or other info. Figuring out how to automate & pull date from the entries is a next step.
### Next Steps

- Write a script that automates the formatting of each file into the expected <artistName>_filename.png format.
- Write a better script to capture and resize screencaptures so that glitch images can easily be generated from videos.

#### Connect with the developer
This application is maintained by Robert Glatzel:
https://twitter.com/rglatzell
